package com.example.administrator.OnLine;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;


public class MyApplication extends Application {
    private static Context mContext;
    @Override
    public void onCreate(){
        super.onCreate();
        mContext = getApplicationContext();
    }
    public static Context getGlobalContext(){
        return mContext;
    }

    /**
    声明定时通知方法供Task调用
    */
    public void sendNotification(String title,String text) {
        //创建大图标的Bitmap
        Bitmap LargeBitmap;
        LargeBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ico);
        //获取NotificationManager实例
        NotificationManager notifyManager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        //实例化NotificationCompat.Builde并设置相关属性
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        //设置小图标
        builder.setSmallIcon(R.mipmap.ico)
                .setLargeIcon(LargeBitmap)
                //设置通知标题
                .setContentTitle(title)
                //设置通知内容
                .setContentText(text)
                //点击自动取消
                .setAutoCancel(true)
                //震动 灯
                .setDefaults( Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setDefaults(Notification.DEFAULT_SOUND);//获取默认铃声
        //设置通知时间，默认为系统发出通知的时间，通常不用设置
        //.setWhen(System.currentTimeMillis());
        //实例化通知栏之后通过给他添加.flags属性赋值。点击后消失
        Notification notification = builder.build();
        notification.flags=Notification.FLAG_AUTO_CANCEL;
        //通过builder.build()方法生成Notification对象,并发送通知,id=1
        notifyManager.notify(1, builder.build());
    }

}
